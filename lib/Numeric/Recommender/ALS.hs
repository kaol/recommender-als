{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Numeric.Recommender.ALS where

import Control.Parallel.Strategies
import Data.Bifunctor
import Data.Default.Class
import qualified Data.Foldable as Foldable
import qualified Data.IntMap.Lazy as IntMap
import qualified Data.IntSet as IntSet
import Data.List (sortBy, sortOn, unfoldr)
import Data.Maybe
import Data.Tuple
import qualified Data.Vector.Storable
import Data.Vector.Storable ((//))
import Numeric.LinearAlgebra
import System.Random

import Prelude hiding ((<>))

data ALSParams = ALSParams
  { lambda :: Double    -- ^ Training speed
  , alpha :: Double     -- ^ Weight multiplier
  , seed :: Int         -- ^ RNG seed
  , nFactors :: Int     -- ^ Hidden features dimension
  , parChunk :: Int     -- ^ Chunk size for parallelization
  } deriving (Show)

instance Default ALSParams where
  def = ALSParams 0.1 10 0 10 10

data ALSResult = ALSResult
  { cost :: Double
  , itemFeature :: !(Matrix Double)
  , userFeature :: !(Matrix Double)
  }

data ALSModel u i = ALSModel
  { encodeUser :: u -> Maybe Int        -- ^ User to dense representation
  , decodeUser :: Int -> u              -- ^ User from dense representation
  , encodeItem :: i -> Maybe Int        -- ^ Item to dense representation
  , decodeItem :: Int -> i              -- ^ Item from dense representation
  -- | Internal representation of the input data pairs
  , pairs :: [(Int, Int)]
  -- | Results as further iterations of the algorithm
  , results :: [ALSResult]
  }

-- | Build recommendations based on users' unrated item choices.
--
-- Takes conversion functions to/from Int representation for user
-- supplied data types.  Use 'id' if you're already based on them.
--
-- The implementation follows the one in the recommenderlab library in
-- CRAN.  For further details, see "Large-scale Parallel Collaborative
-- Filtering for the Netflix Prize" by Yunhong Zhou, Dennis Wilkinson,
-- Robert Schreiber and Rong Pan.
buildModel
  :: (Functor f, Foldable f)
  => ALSParams
  -> (u -> Int)
  -> (Int -> u)
  -> (i -> Int)
  -> (Int -> i)
  -> f (u, i)      -- ^ User-item pairs
  -> ALSModel u i
buildModel p fromUser toUser fromItem toItem =
  buildModelRated p fromUser toUser fromItem toItem .
  fmap (\(a,b) -> (a,(b,1)))

-- | Build model for data with ratings.
buildModelRated
  :: (Functor f, Foldable f)
  => ALSParams
  -> (u -> Int)
  -> (Int -> u)
  -> (i -> Int)
  -> (Int -> i)
  -> f (u, (i, Double))  -- ^ User-item pairs, rated
  -> ALSModel u i
buildModelRated ALSParams{..} fromUser toUser fromItem toItem xs = let
  parMap' f = withStrategy (parListChunk parChunk rdeepseq) . map f
  rnd = mkStdGen seed
  (encUser, decUser) = bimap (. fromUser) (toUser .) .
    compact $ fmap (fromUser . fst) xs
  (encItem, decItem) = bimap (. fromItem) (toItem .) .
    compact $ fmap (fromItem . fst . snd) xs
  xs' = fmap (bimap (fromJust . encUser) (first (fromJust . encItem))) xs
  nU = 1 + (maximum $ fmap fst xs')
  nM = 1 + (maximum $ fmap (fst . snd) xs')
  selRated = (Data.Vector.Storable.replicate (nU*nM) 0.0) //
             (map (\(u,(c,v)) -> (c+(nM*u), v)) $ Foldable.toList xs')
  ratings = reshape nM selRated
  ratings' = tr ratings
  weighted = scalar alpha * ratings
  weighted' = tr weighted
  -- Initialize the first row with average ratings
  mIni = (nFactors><nM) $
         (map (\i -> uncurry (/) . fmap fromIntegral $ foldr
                (\x (acc,cnt) -> let v = selRated ! x
                                 in if v > 0.1 then (acc+v,succ cnt) else (acc, cnt))
                (0.0,0) [i, i+nM..nM*nU-1]
              ) [0..nM-1]) ++
         (take (nFactors*(nM-1)) $ randomRs (0,lambda) rnd)
  sumsU = vector $ map (Data.Vector.Storable.foldr (+) 0) $ toRows ratings
  sumsM = vector $ map (Data.Vector.Storable.foldr (+) 0) $ toColumns ratings
  factorsIdent = ident nFactors
  minimizeLoss :: Int -> Int -> Vector Double -> Matrix Double -> Matrix Double -> Matrix Double -> Matrix Double
  minimizeLoss n n' sums w r m =
    let mtm = m <> tr m
    in fromRows $ flip parMap' [0..n-1] $ \i ->
      let -- Drop the rows and columns not relevant to this user
          relevant = filter (\j -> (>0.1) $ atIndex r (i,j)) [0..n'-1]
          m' = m ¿ relevant
          f' :: Matrix Double -> Vector Double
          f' x = vector $ map (\j -> atIndex x (i,j))
                 relevant
          w' = f' w
          r' = f' r
          m'' = tr $ (tr m') * asColumn w'
          x1 = mtm + (m'' <> tr m' +
                      (scalar lambda * scalar (atIndex sums i) * factorsIdent))
          x2 = asColumn $ (m'' + m') #> r'
      in flatten . maybe (linearSolveSVD x1 x2) id $ linearSolve x1 x2
  f =
    ((,) <*> tr . minimizeLoss nM nU sumsM weighted' ratings' . tr) .
    minimizeLoss nU nM sumsU weighted ratings

  results = iterate ((\x -> x `seq` f x) . snd) (f mIni)
  in ALSModel encUser decUser encItem decItem (foldr ((:) . second fst) [] xs') $
     map (\(u, m) -> ALSResult
           (costFunction ratings u m weighted lambda sumsU sumsM) m u) results
  where
    -- |Build to/from functions from a sparse set to a dense 0..n-1
    -- range.
    --
    -- The reverse function is total for convenience since the inputs
    -- for it are better controlled.
    compact
      :: Foldable f
      => f Int
      -> (Int -> Maybe Int, Int -> Int)
    compact ys = let
      mp = foldr (\x a -> IntMap.insertWith (flip const) x (IntMap.size a) a) mempty ys
      pm = IntMap.fromList . map swap $ IntMap.toList mp
      in ( flip IntMap.lookup mp
         , maybe (error $ "missing value") id . flip IntMap.lookup pm
         )
    -- |This is not actually used by the algorithm.
    costFunction
      :: Matrix Double -> Matrix Double -> Matrix Double -> Matrix Double
      -> Double -> Vector Double -> Vector Double -> Double
    costFunction r u m w l nui nmj = let rum = r - (u <> m)
      in sumElements ((w + 1) * (rum * rum)) +
         (l * (sumElements (nui <# (u^2)) + sumElements ((m^2) #> nmj)))

-- | Per user recommendations.  Results include items user has
-- selected and the snd in the tuple is False for those items.
--
-- The result IntMap uses dense user representation as the key.
-- Access with encodeUser.
recommend
  :: ALSModel u i
  -> Int  -- ^ Iterations
  -> IntMap.IntMap [(i, Bool)]
recommend ALSModel{..} n =
  let ALSResult{..} = results !! (n-1)
      feat = userFeature <> itemFeature
      usrIt = foldr
        (\(k,v) -> IntMap.insertWith IntSet.union k (IntSet.singleton v))
        mempty pairs
  in foldr (\u -> let inUsr = fromJust $ IntMap.lookup u usrIt in
                    IntMap.insert u $
                    map ((\x -> (decodeItem x, not $ IntSet.member x inUsr)) . fst) $
                    sortBy (\(_,a) (_,b) -> compare b a) $
                    zip [0..] $ head $ toLists $ feat ? [u])
     mempty $ map fst pairs

-- | The feature matrix gives nFactors-dimensional coordinates for the
-- items.  As such similar results can be found with using norm.
--
-- The result IntMap uses dense item representation as the key.
-- Access with encodeItem.
related
  :: ALSModel u i
  -> Int -- ^ Iterations
  -> IntMap.IntMap [(i, Double)] -- ^ IntMap key encoded item
related ALSModel{..} n =
  let ALSResult{..} = results !! (n-1)
  in IntMap.fromAscList .
     map (\((i,v),xs) ->
             (i, sortOn snd $ map (\(a,b) -> (decodeItem a, norm_2 $ v - b)) xs)
         ) .
     (let f (_,[]) = Nothing
          f (rs, x:xs) = Just ((x,(rs++xs)), (x:rs, xs))
      in unfoldr f . ([],)) .
     zip [0..] .
     toColumns $ itemFeature

-- | Sort items by components of the feature matrix.  This could be
-- used for visualization purposes or to sort items along some feature
-- axis.  The interpretation of them is totally up to the user but
-- it's not unreasonable to expect items close to each other in this
-- representation to be in some sense similar.
--
-- There's no guarantee that the results are stable between different
-- models even with only small differences.
byDimensions
  :: ALSModel u i
  -> Int
  -> [[(i, Double)]]
byDimensions ALSModel{..} n =
  let ALSResult{..} = results !! (n-1)
  in map (map (\(a,b) -> (decodeItem a, b)) . sortOn snd . zip [0..] . toList) $
     toRows itemFeature
