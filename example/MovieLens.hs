{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (forM_)
import qualified Data.IntMap as IntMap
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Vector as V
import Numeric.Recommender.ALS
import qualified MovieLens.CLI as CLI
import MovieLens.Types

main = do
  cfg <- CLI.initialize
  -- Used only to display
  let movies = IntMap.fromList $ map ((,) <$> movieID <*> movieTitle) $ V.toList $
               CLI.movies cfg
      movieName = fromMaybe "missing???" . flip IntMap.lookup movies
      -- Shape the input data for model building
      ratings = V.map ((,) <$> userID <*> ((,) <$> userMovieID <*> userRating)) $
                CLI.ratings cfg
      -- The model is lazy.  Access the results list for as many
      -- iterations you need.
      model = buildModelRated (CLI.alsParams cfg) id id id id ratings
      iterations = CLI.iterations cfg

  case CLI.action cfg of
    CLI.UserRecommends u -> do
      let xs = map movieName $ take 10 $
               map fst $ filter snd $
               fromMaybe [] $
               flip IntMap.lookup (recommend model iterations) =<< encodeUser model u
      mapM_ (putStrLn . T.unpack) xs
    CLI.RelatedMovies m -> do
      let xs = map movieName $ take 10 $
               map fst $ filter ((> 0.01) . snd) $
               fromMaybe [] $
               flip IntMap.lookup (related model iterations) =<< encodeItem model m
      mapM_ (putStrLn . T.unpack) xs
    CLI.Dimension d -> do
      let xs = map (\(a,b) -> (movieName a, b)) $
               byDimensions model iterations !! d
      forM_ xs $ \(title, val) -> do
        putStrLn $ show val ++ "\t" ++ T.unpack title
