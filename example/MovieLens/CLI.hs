module MovieLens.CLI where

import Data.Default.Class (def)
import Data.Vector (Vector)
import Options.Applicative
import MovieLens.Types
import Numeric.Recommender.ALS (ALSParams(..))

data ExampleMode
  = UserRecommends Int
  | RelatedMovies Int
  | Dimension Int

type Options = (FilePath, FilePath, Int, Int, Int, ExampleMode)

movieLensOptions :: Parser Options
movieLensOptions =
  (,,,,,)
  <$> strArgument (metavar "MOVIES")
  <*> strArgument (metavar "RATINGS")
  <*> option auto (short 'd' <> long "dimensions" <> value 10 <> showDefault)
  <*> option auto (short 'i' <> long "iterations" <> value 10 <> showDefault)
  <*> option auto (short 's' <> long "seed" <> value 0)
  <*> subparser
    ((command "recommend"
      (info (UserRecommends <$> argument auto (metavar "userID"))
       (progDesc "Recommend movies to a user")
      )) <>
     (command "related"
      (info (RelatedMovies <$> argument auto (metavar "movieID"))
       (progDesc "Get movies most similar to a movie")
      )) <>
      (command "dimension"
       (info (Dimension <$> argument auto (metavar "dimension"))
        (progDesc "Sort movies by feature matrix dimension")
      )))

data InitialState = InitialState
  { action :: ExampleMode
  , iterations :: Int
  , alsParams :: ALSParams
  , movies :: Vector Movie
  , ratings :: Vector Rating
  }

initialize :: IO InitialState
initialize = do
  (moviesFile, ratingsFile, nFactors, it, seed, exampleMode) <- execParser opts
  InitialState exampleMode it (def {seed = seed, nFactors = nFactors})
    <$> decodeMovies moviesFile
    <*> decodeRatings ratingsFile
  where
    opts = info (movieLensOptions <**> helper)
      ( fullDesc
        <> progDesc "Example recommendations program based on MovieLens dataset"
        <> footer "Get data from https://grouplens.org/datasets/movielens/"
      )
