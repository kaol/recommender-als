{-# LANGUAGE DeriveGeneric #-}

module MovieLens.Types where

import Control.Monad
import Data.ByteString.Lazy as BL
import Data.Csv
import Data.Text
import Data.Vector (Vector)
import GHC.Generics (Generic)

data Movie = Movie
  { movieID :: !Int
  , movieTitle :: !Text
  , movieGenres :: !Text
  }
  deriving (Generic, Show)

instance FromRecord Movie

data Rating = Rating
  { userID :: !Int
  , userMovieID :: !Int
  , userRating :: !Double
  , userTimestamp :: !Int
  }
  deriving (Generic, Show)

instance FromRecord Rating

decodeMovies :: FilePath -> IO (Vector Movie)
decodeMovies = either error return . decode HasHeader <=< BL.readFile

decodeRatings :: FilePath -> IO (Vector Rating)
decodeRatings = either error return . decode HasHeader <=< BL.readFile

-- Don't use "error" in your own programs.
