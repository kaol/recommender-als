# Revision history for recommender-als

## 0.2.2.0 -- 2024-10-13

* Add an example based on MovieLens data.
* Add functions related and byDimensions.
* Optimizations to data initialization and loss minimization.

## 0.2.1.1 -- 2020-08-26

* Add missing enum to mIni construction.

## 0.2.1.0 -- 2020-08-26

* Add buildModelRated.

## 0.2.0.0 -- 2020-07-21

* Expose user and item feature matrices in the model.
* Use parListChunk and parametrize its use for better parallelization.

## 0.1.0.0 -- 2020-07-09

* First version.
