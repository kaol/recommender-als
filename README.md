This package provides a recommendation algorithm based on alternating
least squares algorithm, as made famous by the Netflix Prize.

It takes as its input a list of user-item pairs and returns a list of
recommendations for each user.  The current implementation is limited
to using unrated pairs.

The algorithm is parallelized and should be quick enough to train the
model within seconds for a few thousand users and items.  Getting
recommendations from a computed model happens nearly instantly.

For implementation details, see "Large-scale Parallel Collaborative
Filtering for the Netflix Prize" by Yunhong Zhou, Dennis Wilkinson,
Robert Schreiber and Rong Pan.
